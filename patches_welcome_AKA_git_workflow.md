
Patches welcome
---------------


Saying that 'seperate' should be spelled as 'separate'
is consider a patch, a contribution.

That patch goes into a git repository.
It will need a commit message.

Sending in code changes with commit message is really great.

Even better are patches that can go easy into git.


Yes, "can go easy into git" is a broad term.


Important is that there should no fear for sending in patches.


Several scenarios are possible.


git format-patch
----------------

What was prepared with `git format-patch`
can be processed with `git am`


public git repository
---------------------

If the patch is in a public readable git repository,
provide the URL  (and branch name)



account on salsa
----------------

If you have a (guest) account on salsa,
use merge request.



group member
------------

Members of salsa arduino group can commit thier patches straightaway.
