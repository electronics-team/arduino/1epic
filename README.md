Welcome at the construction site

## How it works on Fedora 33

### source: arduino-1.8.13-2
- binaries (noarch)
  - arduino-1.8.13-2
  - arduino-core-1.8.13-2
  - arduino-devel-1.8.13-2
  - arduino-doc-1.8.13-2
- requires
  - arduino-core
  - arduino-devel
  - javapackages-tools
  - polkit
- build-requires:
  - ant
  - desktop-file-utils
  - libappstream-glib
  - javapackages-tools
  - java-devel
  - mvn(com.fasterxml.jackson.core:jackson-annotations)
  - mvn(com.fasterxml.jackson.core:jackson-core)
  - mvn(com.fasterxml.jackson.core:jackson-databind)
  - mvn(com.fifesoft:rsyntaxtextarea)
  - mvn(com.github.zafarkhaja:java-semver)
  - mvn(com.jcraft:jsch)
  - mvn(commons-codec:commons-codec)
  - mvn(commons-io:commons-io)
  - mvn(org.apache.commons:commons-compress)
  - mvn(org.apache.commons:commons-exec)
  - mvn(org.apache.commons:commons-lang3)
  - mvn(org.apache.commons:commons-logging)
  - mvn(org.apache.commons:commons-net)
  - mvn(org.apache.logging.log4j:log4j-api)
  - mvn(org.apache.xmlgraphics:batik-all)
  - mvn(org.bouncycastle:bcpg-jdk15)
  - mvn(org.jmdns:jmdns)
  - mvn(org.scream3r:jssc)


### source: arduino-builder-1.3.25-7
- binaries x86_64
  - arduino-builder-1.3.25-7
  - arduino-builder-debuginfo-1.3.25-7
  - arduino-builder-debugsource-1.3.25-7
- requires
  - arduino-ctags
- build-requires:
  - gcc
  - golang >= 1.4.3
  - git
  - golang(github.com/go-errors/errors)
  - golang(github.com/stretchr/testify) # Needed for unit tests


### source: arduino-ctags-5.8-12.arduino11
- binaries x86_64
  - arduino-ctags-5.8-12.arduino11
  - arduino-ctags-debuginfo-5.8-12.arduino11
  - arduino-ctags-debugsource-5.8-12.arduino11
